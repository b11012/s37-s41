/*User
firstName - string,
lastName - string,
email - string,
password - string,
mobileNo- string,
isAdmin - boolean,
		default: false
enrollment: [
	{
		courseId: string,
		status: string,
		dateEnrolled: date
	}
]*/

const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile no. is required"]
	},

	isAdmin :{
		type: Boolean,
		default: false
	},

	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course Id is required"]
			},

			status: {
				type: String,
				default: "Enrolled"
			},

			dateEnrolled: {
				type: Date,
				default: new Date()
			}	
		}
	] 
})

module.exports = mongoose.model("User", userSchema);