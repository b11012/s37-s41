// DEPENDENCIES
const Course = require('../models/Course');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// REGISTER COURSES
module.exports.addCourse = (req, res) =>{

	console.log(req.body);


	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));
};

//  RETRIEVE ALL COURSES
module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// GET SINGLE COURSE

module.exports.getSingleCourseController = (req, res) => {

	console.log(req.params);

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

// [SECTION] UPDATING A COURSE

module.exports.updateCourse = (req, res) => {

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}


Course.findByIdAndUpdate(req.params.id, updates, {new:true})
.then(updateCourse => res.send(updateCourse))
.catch(err => res.send(err))

}

// [ACTIVITY] DEACTIVATE COURSE

module.exports.updateDeactivate = (req, res) => {

	console.log(req.user.id); 

	console.log(req.params.id);

	let updates = {

		isActive: false 
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedDeactivate => res.send(updatedDeactivate))
	.catch(err => res.send(err))
};

// [ACTIVITY] ACTIVATE COURSE

module.exports.updateActivate = (req, res) => {

	console.log(req.user.id); 

	console.log(req.params.id);

	let updates = {

		isActive: true 
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedActive => res.send(updatedActive))
	.catch(err => res.send(err))
};

// RETRIEVE ALL ACTIVE COURSES

module.exports.retrieveActiveCourses = (req, res) => {

	console.log(req.body)

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [SECTION] FIND COURSES BY NAME

module.exports.findCoursesByName = (req, res) => {

	console.log(req.body) // contain the name of the course you are looking for

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send('No courses found')
		
		} else {

			return res.send(result)
		}
	})
}
