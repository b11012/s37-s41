//[SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const courseControllers = require('../controllers/courseControllers');

const auth = require('../auth');

const {verify, verifyAdmin} = auth;

// [SECTION] ROUTES

// ADD COURSE
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

// GET ALL COURSE
router.get('/', courseControllers.getAllCourses);

// GET SINGLE COURSE
router.get('/getSingleCourse/:id', courseControllers.getSingleCourseController);

// update a course
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse)

// [ACTIVITY] Deactivating a course
router.put('/archive/:id', verify, verifyAdmin, courseControllers.updateDeactivate)

// [ACTIVITY] Activating a course
router.put('/activate/:id', verify, verifyAdmin, courseControllers.updateActivate)

// [ACTIVITY] RETRIEVING ACTIVE COURSES
router.get('/getActiveCourses', courseControllers.retrieveActiveCourses)

// find courses by name
router.post('/findCoursesByName', courseControllers.findCoursesByName)

module.exports = router;