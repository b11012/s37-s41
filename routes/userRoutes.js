//[SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const userControllers = require('../controllers/userControllers');
const auth = require('../auth')

// object destructuring from auth module
const {verify, verifyAdmin} = auth;

// [SECTION] ROUTES

// register user
router.post('/', userControllers.registerUser);

// get all user
router.get('/', userControllers.getAllUsers);

// login user
router.post('/login', userControllers.loginUser);

// get user details

router.get('/getUserDetails', verify, userControllers.getUserDetails)

// get single user
router.get('/getSingleUser/:id', userControllers.getSingleUserController);

// check if email exists
router.post('/checkEmailExists', userControllers.checkEmailExists);

// Updating user details

router.put('/updateUserDetails', verify, userControllers.updateUserDetails)

// Update isAdmin details
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin)

// Enrolling User
router.post('/enroll', verify, userControllers.enroll);

// get enrollments
router.get('/getEnrollments', verify, userControllers.getEnrollments)

module.exports = router;

